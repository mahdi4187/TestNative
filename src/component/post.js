import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native'

export default class Post extends Component {

    render() {
        const {image, title, body} = this.props;
        return (
            <View style={styles.container}>
                <Image source={image} style={styles.image}/>
                <Text style={styles.title}>{title}</Text>
                <Text numberOfLines={4} style={styles.body}>{body}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
        marginTop: 25,
        backgroundColor: '#fff',
        elevation: 1,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 20},
        shadowOpacity: 1,
    },
    image: {
        width: '100%',
        height: 150,
    },
    title: {
        textAlign: 'right',
        fontWeight:'500',
        fontSize:18,
        padding:5,
        // fontFamily:'IRANSansBold'
    },
    body: {
        textAlign: 'right',
        fontSize:14,
        padding:5
    }
});