import React from 'react';
import {Text, TextInput, TouchableNativeFeedback, View} from "react-native";
import styles from './../assets/css/Login';
import NavigationBar from 'react-native-navbar';

export default class Login extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={{title: 'ورود', tintColor: '#000'}}
                    // tintColor="#ef5350"
                    statusBar={{style: 'light-content', tintColor: '#00ACC1'}}
                    rightButton={{title: 'منو', tintColor: '#000'}}
                    leftButton={{title: 'بازگشت', tintColor: '#000'}}
                />
                <View style={styles.main}>
                    <View style={styles.panel}>
                        <Text style={styles.titlePanel}>ورود</Text>
                        <View style={styles.inputGroup}>
                            <Text style={styles.label}>ایمیل: </Text>
                            <TextInput
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                placeholder="لطفا ایمیل خود را وارد کنید"
                            />
                        </View>
                        <View style={styles.inputGroup}>
                            <Text style={styles.label}>رمز عبور: </Text>
                            <TextInput
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                placeholder="لطفا رمز خود را وارد کنید"
                                secureTextEntry={true}
                            />
                        </View>
                        <TouchableNativeFeedback
                        onPress={()=>{
                            console.log('btn clicked');
                        }}>
                            <View style={styles.button}>
                                <Text style={styles.btnTex}>ورود به اپلیکیشن</Text>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                </View>
            </View>
        )
    }
}