import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({

    container: {
        flex: 1,
        marginTop:20,

    },
    main: {
        backgroundColor: '#26C6DA',
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        justifyContent:'center',
        alignItems:'center'
    },
    panel: {
        backgroundColor: '#FAFAFA',
        width: 150,
        flex: 1,
        marginRight: 50,
        marginLeft: 50,
        elevation:10
    },
    titlePanel: {
        fontWeight: '500',
        textAlign: 'center',
        marginTop:20,
    },
    inputGroup:{
        padding:5,
        margin:10,
    },
    label:{
        textAlign:'right',
    },
    input:{
        borderColor:'#000',
        borderWidth:1,
        borderRadius:5,
        textAlign:'right',
        marginTop:5,
        marginBottom:5,
        padding:5,
    },
    button:{
        backgroundColor:'#0288D1',
        alignItems:'center',
        marginBottom:20,
        marginRight:20,
        marginLeft:20,
        borderRadius:10,
        padding:10,
        elevation:2,
    },
    btnTex:{
        textAlign:'center',
        color:'#fff',
        fontWeight:'600',
    }

});
export default styles;